CREATE TABLE public.technology
(
    id bigserial NOT NULL,
    name character varying,
    PRIMARY KEY (id)
    UNIQUE (name)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.technology
    OWNER to postgres;
	
	
CREATE TABLE public.resume
(
    id bigserial NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    middle_name character varying,
    dob date,
    sex "char",
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.resume
    OWNER to postgres;
	

CREATE TABLE public.contact
(
    resume_id bigint,
    name character varying,
    CONSTRAINT resume_contact FOREIGN KEY ("resumeId")
        REFERENCES public.resume (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.contact
    OWNER to postgres;
	
CREATE TABLE public.resume_technology
(
    resume_id bigint,
    technology_id bigint,
    CONSTRAINT resume_id FOREIGN KEY ("resumeId")
        REFERENCES public.resume (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT technology_id FOREIGN KEY ("technologyId")
        REFERENCES public.technology (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.resume_technology
    OWNER to postgres;

INSERT INTO public.resume(
	first_name, last_name, middle_name, dob, sex)
	VALUES ( 'Петр' , 'Петров', 'Петрович', '1986-12-12', 'm');

INSERT INTO public.resume(
	first_name, last_name, middle_name, dob, sex)
	VALUES ( 'Иван' , 'Иванов', 'Иванович', '1997-4-4', 'm');

INSERT INTO public.resume(
	first_name, last_name, middle_name, dob, sex)
	VALUES ( 'Мария' , 'Морская', 'Васильевна', '1999-11-7', 'f');
	
INSERT INTO public.contact(
	resume_id, name)
	VALUES (1, '+375(29)123-45-67'), 
	(1, 'http://github.com/petya'), 
	(1, 'petrovich@gmail.com'), 
	(2, '+375(29)287-65-43'), 
	(2, 'http://github.com/vanya'), 
	(2, 'skype:ivanko'), 
	(3, '+375(29)999-99-99'), 
	(3, 'https://www.linkedin.com/in/mariya/');

INSERT INTO public.technology(
	 name)
	VALUES ('Git'), 
	('Spring Boot'), 
	('HTML'), 
	('JavaEE'), 
	('Java Core'), 
	('Maven'), 
	('REST'), 
	('Spring');	
	
INSERT INTO public.resume_technology(
	resume_id, technology_id)
	VALUES (1, 1),
	(1, 2),
	(1, 3),
	(2, 1),
	(2, 4),
	(2, 5),
	(3, 6),
	(3, 7),
	(3, 8);