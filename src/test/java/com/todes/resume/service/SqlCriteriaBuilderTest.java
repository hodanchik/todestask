package com.todes.resume.service;

import com.todes.resume.dao.Specification;
import com.todes.resume.entity.Resume;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SqlCriteriaBuilderTest {

    private static CriteriaBuilder criteriaBuilder;

    @Before
    public void setUp() {
        criteriaBuilder = new SqlCriteriaBuilder();
    }

    @Test
    public void buildQuery() {
        Specification specForFind = criteriaBuilder.init(Resume.class).and().equal("last_name", "Морская")
                .and().equal("first_name", "Мария")
                .and().equal("middle_name", "Васильевна").build();
       String expectedQuery = "SELECT DISTINCT Resume.id, Resume.first_name, resume.middle_name, resume.last_name," +
               "resume.dob, resume.sex FROM resume JOIN contact on resume.id = contact.resume_id " +
               "JOIN resume_technology on resume.id = resume_technology.resume_id JOIN technology " +
               "on resume_technology.technology_id = technology.id WHERE 1=1 AND  last_name = 'Морская' " +
               "AND  first_name = 'Мария' AND  middle_name = 'Васильевна'";
        Assert.assertEquals(expectedQuery, specForFind.getQuery());
    }

    @Test
    public void buildParameters() {
        Specification specForFind = criteriaBuilder.init(Resume.class).and().equal("last_name", "Морская")
                .and().equal("first_name", "Мария")
                .and().equal("middle_name", "Васильевна").build();
        List<SearchCriteria> expectedParams = new ArrayList<>();
        expectedParams.add(new SearchCriteria("last_name", OperationType.EQUALS, "Морская"));
        expectedParams.add(new SearchCriteria("first_name", OperationType.EQUALS, "Мария"));
        expectedParams.add(new SearchCriteria("middle_name", OperationType.EQUALS, "Васильевна"));
        Assert.assertEquals(expectedParams, specForFind.getParams());
    }
}