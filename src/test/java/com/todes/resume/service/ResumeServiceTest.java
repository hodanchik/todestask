package com.todes.resume.service;

import com.todes.resume.dao.ResumeDao;
import com.todes.resume.dao.Specification;
import com.todes.resume.dao.TechnologyDao;
import com.todes.resume.entity.Resume;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ResumeServiceTest {

    private ResumeService resumeService;
    private ResumeDao mockResumeDao;
    private TechnologyDao mockTechnologyDao;
    private CriteriaBuilder criteriaBuilder;
    private Specification specForFind;

    @Before
    public void setUp() {
        criteriaBuilder = new SqlCriteriaBuilder();
        specForFind = criteriaBuilder.init(Resume.class).and().equal("last_name", "Морская")
                .and().equal("first_name", "Мария")
                .and().equal("middle_name", "Васильевна").build();
        mockResumeDao = Mockito.mock(ResumeDao.class);
        mockTechnologyDao = Mockito.mock(TechnologyDao.class);
        resumeService = new ResumeServiceImpl(mockResumeDao, mockTechnologyDao);
    }

    @Test
    public void find() {
        resumeService.find(specForFind);
        Mockito.verify(mockResumeDao, Mockito.times(1)).find(specForFind);
    }
}