package com.todes.resume.dao;

import com.todes.resume.entity.Resume;
import com.todes.resume.service.CriteriaBuilder;
import com.todes.resume.service.SqlCriteriaBuilder;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@RunWith(JUnit4.class)
public class ResumeDaoImplTest {
    private static Connection connection;
    private static CriteriaBuilder criteriaBuilderOne;
    private static CriteriaBuilder criteriaBuilderTwo;
    private static Specification specForFindOne;
    private static Specification specForFindTwo;
    private ResumeDao resumeDao;

    @BeforeClass
    public static void initConnection() throws SQLException, ClassNotFoundException {

        Class.forName("org.postgresql.ds.PGSimpleDataSource");
        connection =
                DriverManager.getConnection("jdbc:h2:mem:resumemanager", "sa", "");
        criteriaBuilderOne = new SqlCriteriaBuilder();
        specForFindOne = criteriaBuilderOne.init(Resume.class).and().equal("last_name", "Морская")
                .and().equal("first_name", "Мария")
                .and().equal("middle_name", "Васильевна").build();
        criteriaBuilderTwo = new SqlCriteriaBuilder();
        specForFindTwo = criteriaBuilderTwo.init(Resume.class).and().endsWith("last_name", "ов")
                .or().equal("sex", "f").build();
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    @Before
    public void createTable() throws SQLException {
        resumeDao = new ResumeDaoImpl();
        String createTechnologyTable = "CREATE TABLE technology (id      bigint         NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                " name    varchar(30) NOT NULL, PRIMARY KEY (id))";
        executeSql(createTechnologyTable);
        String createResumeTable = "CREATE TABLE resume (id bigint NOT NULL PRIMARY KEY AUTO_INCREMENT," +
                " first_name varchar(30), last_name varchar(30), middle_name varchar(30) NOT NULL, dob date NOT NULL, " +
                " sex varchar(1), PRIMARY KEY (id))";
        executeSql(createResumeTable);
        String createResumeTechnoTable = "CREATE TABLE resume_technology (resume_id bigint NOT NULL, technology_id bigint NOT NULL,\n" +
                "    primary key (resume_id, technology_id),\n" +
                "    constraint fk_resume_id\n" +
                "        foreign key (resume_id) references resume (id) ,\n" +
                "    constraint fk_technology_id\n" +
                "        foreign key (technology_id) references technology (id)\n" +
                ");";
        executeSql(createResumeTechnoTable);
        String createContactTable = "CREATE TABLE contact (resume_id bigint NOT NULL," +
                " name varchar(50), " +
                " constraint fk_contact_resume_id  foreign key (resume_id) references resume (id))";
        executeSql(createContactTable);
        String insertResume = "INSERT INTO resume(first_name, last_name, middle_name, dob, sex)" +
                " VALUES ( 'Петр' , 'Петров', 'Петрович', '1986-12-12', 'm')," +
                "( 'Иван' , 'Иванов', 'Иванович', '1997-4-4', 'm')," +
                "( 'Мария' , 'Морская', 'Васильевна', '1999-11-7', 'f');";
        executeSql(insertResume);
        String insertContact = " INSERT INTO contact(resume_id, name)\n" +
                "            VALUES (1, '+375(29)123-45-67'),\n" +
                "             (1, 'http://github.com/petya'),\n" +
                "            (1, 'petrovich@gmail.com'),\n" +
                "            (2, '+375(29)287-65-43'),\n" +
                "            (2, 'http://github.com/vanya'),\n" +
                "            (2, 'skype:ivanko'),\n" +
                "            (3, '+375(29)999-99-99'),\n" +
                "            (3, 'https://www.linkedin.com/in/mariya/');";
        executeSql(insertContact);
        String insertTechnology = " INSERT INTO technology( name)\n" +
                "          VALUES ('Git'),\n" +
                "                    ('Spring Boot'),\n" +
                "                    ('HTML'),\n" +
                "                    ('JavaEE'),\n" +
                "                    ('Java Core'),\n" +
                "                    ('Maven'),\n" +
                "                    ('REST'),\n" +
                "                    ('Spring');";
        executeSql(insertTechnology);
        String insertResumeTechnology = " INSERT INTO resume_technology( resume_id, technology_id)\n" +
                "  VALUES (1, 1),\n" +
                "                    (1, 2),\n" +
                "            (1, 3),\n" +
                "            (2, 1),\n" +
                "            (2, 4),\n" +
                "            (2, 5),\n" +
                "            (3, 6),\n" +
                "            (3, 7),\n" +
                "            (3, 8);";
        executeSql(insertResumeTechnology);
    }

    @After
    public void dropTable() throws SQLException {
        String sqlTechnology = "drop table technology";
        String sqlResume = "drop table resume";
        String sqlResumeTechno = "drop table resume_technology";
        String sqlContact = "drop table contact";
        executeSql(sqlTechnology);
        executeSql(sqlResume);
        executeSql(sqlResumeTechno);
        executeSql(sqlContact);
    }

    @Test
    public void findResumeBySpecOne() {
        List<Resume> resumes = resumeDao.find(specForFindOne);
        Assert.assertEquals(1, resumes.size());
        Resume actualResume = resumes.get(0);
        long expectedResumeId = 3L;
        Assert.assertEquals(expectedResumeId, actualResume.getId());
    }

    @Test
    public void findResumeBySpecTwo() {
        List<Resume> resumes = resumeDao.find(specForFindTwo);
        Assert.assertEquals(3, resumes.size());
    }

    private void executeSql(String sql) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
        statement.close();
    }
}