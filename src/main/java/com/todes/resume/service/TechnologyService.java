package com.todes.resume.service;
import com.todes.resume.entity.Technology;

import java.util.List;

public interface TechnologyService {
    void save(Technology technology);

    boolean update(Technology technology) ;

    Technology getById(Long id) ;

    List<Technology> findAll();

}
