package com.todes.resume.service;
import com.todes.resume.dao.TechnologyDao;
import com.todes.resume.entity.Technology;

import java.util.List;

public class TechnologyServiceImpl implements TechnologyService{
    private TechnologyDao technologyDao;

    public TechnologyServiceImpl(TechnologyDao technologyDao) {
        this.technologyDao = technologyDao;
    }

    @Override
    public void save(Technology technology) {
        technologyDao.save(technology);
    }

    @Override
    public boolean update(Technology technology) {
        return technologyDao.update(technology);
    }

    @Override
    public Technology getById(Long id) {
        return technologyDao.getById(id);
    }

    @Override
    public List<Technology> findAll() {
        return  technologyDao.findAll();
    }
}
