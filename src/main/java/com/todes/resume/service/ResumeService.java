package com.todes.resume.service;

import com.todes.resume.dao.Specification;
import com.todes.resume.entity.Resume;

import java.util.List;

public interface ResumeService {

    void save(Resume resume);

    boolean update(Resume resume) ;

    boolean delete(Long id) ;

    Resume getById(Long id) ;

    List<Resume> findAll();

    List<Resume> find(Specification specification) ;
}
