package com.todes.resume.service;

public enum OperationType {
    ENDS_WITH, STARTS_WITH, EQUALS
}
