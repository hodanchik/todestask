package com.todes.resume.service;

import com.todes.resume.dao.ResumeDao;
import com.todes.resume.dao.Specification;
import com.todes.resume.dao.TechnologyDao;
import com.todes.resume.entity.Resume;
import com.todes.resume.entity.Technology;

import java.util.List;
import java.util.Set;

public class ResumeServiceImpl implements ResumeService {
    private ResumeDao resumeDao;
    private TechnologyDao technologyDao;

    public ResumeServiceImpl(ResumeDao resumeDao, TechnologyDao technologyDao) {
        this.resumeDao = resumeDao;
        this.technologyDao = technologyDao;
    }

    @Override
    public void save(Resume resume) {
        Set<Technology> technologies = resume.getTechnologies();
        for(Technology technology: technologies){
            technologyDao.save(technology);
            long technologyId = technologyDao.getTechnologyByName(technology.getName()).getId();
            technology.setId(technologyId);
        }
        resumeDao.save(resume);
    }

    @Override
    public boolean update(Resume resume) {
        Set<Technology> technologies = resume.getTechnologies();
        for(Technology technology: technologies){
            technologyDao.save(technology);
            long technologyId = technologyDao.getTechnologyByName(technology.getName()).getId();
            technology.setId(technologyId);
        }
        return resumeDao.update(resume);
    }

    @Override
    public boolean delete(Long id) {
        return resumeDao.delete(id);
    }

    @Override
    public Resume getById(Long id) {
        return resumeDao.getById(id);
    }

    @Override
    public List<Resume> findAll(){
        return resumeDao.findAll();
    }

    @Override
    public List<Resume> find(Specification specification) {
        return resumeDao.find(specification);
    }
}
