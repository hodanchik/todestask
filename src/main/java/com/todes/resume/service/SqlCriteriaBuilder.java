package com.todes.resume.service;

import com.todes.resume.dao.Specification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlCriteriaBuilder implements CriteriaBuilder {

    public static final String PERCENT = "%";
    public static final String S_QUOTE = "'";
    private StringBuilder stringQuery;
    private List<SearchCriteria> params;
    private Map<String, String> initQueryMap ;

    public SqlCriteriaBuilder() {
        params = new ArrayList<>();
        initQueryMap = new HashMap<>();
        stringQuery = new StringBuilder();
        fillQueryMap();
    }

    private void fillQueryMap() {
        initQueryMap.put("Resume", "SELECT DISTINCT resume.id, resume.first_name, resume.middle_name, resume.last_name," +
                "resume.dob, resume.sex FROM resume JOIN contact on resume.id = contact.resume_id " +
                "JOIN resume_technology on resume.id = resume_technology.resume_id " +
                "JOIN technology on resume_technology.technology_id = technology.id WHERE 1=1");
    }

    @Override
    public Specification build() {
        return new Specification(stringQuery.toString(), params);
    }

    @Override
    public CriteriaBuilder init(Class clazz) {
        String className = clazz.getSimpleName();
        String selectQuery = initQueryMap.get(className);
        stringQuery.append(selectQuery);
        return this;
    }

    @Override
    public CriteriaBuilder and() {
        stringQuery.append(" AND ");
        return this;
    }

    @Override
    public CriteriaBuilder or() {
        stringQuery.append(" OR ");
        return this;
    }

    @Override
    public CriteriaBuilder equal(String field, String value) {
        stringQuery.append(" " + field + " = " + S_QUOTE + value + S_QUOTE);
        params.add(new SearchCriteria(field, OperationType.EQUALS, value));
        return this;
    }

    @Override
    public CriteriaBuilder endsWith(String field, String value) {
        stringQuery.append(" " + field + " LIKE " + S_QUOTE + PERCENT + value + S_QUOTE);
        params.add(new SearchCriteria(field, OperationType.ENDS_WITH, value));
        return this;
    }

    @Override
    public CriteriaBuilder startsWith(String field, String value) {
        stringQuery.append(" " + field + " LIKE " + S_QUOTE + value + PERCENT + S_QUOTE);
        params.add(new SearchCriteria(field, OperationType.STARTS_WITH, value));
        return this;
    }
}
