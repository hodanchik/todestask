package com.todes.resume.service;

import com.todes.resume.dao.Specification;
public interface CriteriaBuilder {
    Specification build();
    CriteriaBuilder init(Class clazz);
    CriteriaBuilder and();
    CriteriaBuilder  or();
    CriteriaBuilder equal(String field, String value);
    CriteriaBuilder endsWith(String field, String value);
    CriteriaBuilder startsWith(String field, String value);
}
