package com.todes.resume.dao;

import com.todes.resume.entity.Technology;
import com.todes.resume.jdbc.ConnectionPoolImpl;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TechnologyDaoImpl implements TechnologyDao {
    private static final Logger LOGGER = Logger.getLogger(TechnologyDaoImpl.class);
    private static final String SAVE_TECHNOLOGY = "INSERT INTO technology(name) VALUES (?) ON CONFLICT DO NOTHING";
    private static final String FIND_TECHNOLOGY_BY_NAME = "SELECT id, name FROM technology WHERE name = ?";
    private static final String FIND_TECHNOLOGY_BY_ID = "SELECT id, name FROM technology WHERE id = ?";
    private static final String GET_ALL_TECHNOLOGIES = "SELECT id, name FROM technology";
    private static final String UPDATE_TECHNOLOGY = "UPDATE technology SET name=? WHERE id = ?";

    @Override
    public void save(Technology technology) {
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SAVE_TECHNOLOGY)) {
            int i = 0;
            preparedStatement.setString(++i, technology.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Request or table failed to save technology with name = " + technology.getName());
        }
    }

    @Override
    public boolean update(Technology technology) {
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_TECHNOLOGY)) {
            int i = 0;
            preparedStatement.setString(++i, technology.getName());
            preparedStatement.setLong(++i, technology.getId());
            int updateString = preparedStatement.executeUpdate();
            return (updateString > 0);
        } catch (SQLException e) {
            LOGGER.error("Request or table failed save technology with name = " + technology.getName());
        }
        return false;
    }

    @Override
    public boolean delete(Long id) {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public Technology getById(Long id) {
        Technology technology = new Technology();
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_TECHNOLOGY_BY_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                technology = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get technology by id = " + id);
        }
        return technology;
    }

    @Override
    public List<Technology> findAll() {
        List<Technology> allTechnologyList = new ArrayList<>();
        try (Connection con = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(GET_ALL_TECHNOLOGIES)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Technology technology = parseResultSet(resultSet);
                allTechnologyList.add(technology);
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get all technologies");
        }
        return allTechnologyList;
    }

    @Override
    public Technology getTechnologyByName(String name) {
        Technology technology = new Technology();
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_TECHNOLOGY_BY_NAME)) {
            int i = 0;
            preparedStatement.setString(++i, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                technology = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed save technology with name = " + technology.getName());
        }
        return technology;
    }

    private Technology parseResultSet(ResultSet resultSet) throws SQLException {
        try {
            long technologyId = resultSet.getLong("id");
            String technologyName = resultSet.getString("name");
            return new Technology(technologyId, technologyName);
        } catch (SQLException e) {
            LOGGER.error("Parse resultSet exception");
            throw new SQLException();
        }
    }
}
