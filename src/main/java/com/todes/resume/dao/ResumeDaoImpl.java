package com.todes.resume.dao;

import com.todes.resume.entity.Resume;
import com.todes.resume.entity.Technology;
import com.todes.resume.jdbc.ConnectionPoolImpl;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ResumeDaoImpl implements ResumeDao {
    private final static Logger LOGGER = Logger.getLogger(ResumeDaoImpl.class);
    private static final String FIND_ALL_CONTACTS_BY_RESUME_ID = "SELECT name FROM contact where resume_id = ?";
    private static final String FIND_ALL_TECHNOLOGY_BY_RESUME_ID = "SELECT resume_id, technology_id, technology.name " +
            "from resume_technology join technology on resume_technology.technology_id = technology.id " +
            "where resume_id = ?";
    private static final String DELETE_RESUME_BY_RESUME_ID = "DELETE from resume where id = ? "; // Cascade at db level
    private static final String GET_RESUME_BY_RESUME_ID = "SELECT id, first_name, last_name, middle_name, dob, sex " +
            "FROM resume WHERE id = ?";
    private static final String GET_ALL_RESUME = "SELECT id, first_name, last_name, middle_name, dob, sex FROM resume";
    private static final String SAVE_RESUME = "INSERT INTO resume(first_name, last_name, middle_name, dob, sex) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String SAVE_RESUME_CONTACT = "INSERT INTO contact(resume_id, name) VALUES (?, ?)";
    private static final String SAVE_RESUME_TECHNOLOGY = "INSERT INTO resume_technology(resume_id, technology_id) " +
            "VALUES (?, ?)";
    private static final String DELETE_RESUME_CONTACT = "DELETE from contact where resume_id = ?";
    private static final String DELETE_RESUME_TECHNOLOGY = "DELETE from resume_technology WHERE resume_id = ? ";

    private static final String UPDATE_RESUME = "UPDATE resume SET first_name=?, last_name=?, middle_name=?, dob=?, sex=? " +
            "WHERE id = ?";

    @Override
    public void save(Resume resume) {
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SAVE_RESUME, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setString(++i, resume.getFirstName());
            preparedStatement.setString(++i, resume.getLastName());
            preparedStatement.setString(++i, resume.getMiddleName());
            preparedStatement.setDate(++i, resume.getDob());
            preparedStatement.setObject(++i, resume.getSex());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                resume.setId(generatedKeys.getLong(1));
                resume.getContacts().forEach(contact -> saveContact(connection, resume.getId(), contact));
                resume.getTechnologies()
                        .forEach(technology -> saveTechnologies(connection, resume.getId(), technology.getId()));
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed save resume ");
        }
    }

    private void saveTechnologies(Connection connection, Long resumeId, Long technologyId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_RESUME_TECHNOLOGY)) {
            int i = 0;
            preparedStatement.setLong(++i, resumeId);
            preparedStatement.setLong(++i, technologyId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error("Request or table failed save technology for resume");
        }
    }

    private void saveContact(Connection connection, Long resumeId, String contact) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SAVE_RESUME_CONTACT)) {
            int i = 0;
            preparedStatement.setLong(++i, resumeId);
            preparedStatement.setString(++i, contact);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error("Request or table failed save contact for resume");
        }
    }

    @Override
    public boolean update(Resume resume) {
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_RESUME)) {
            int i = 0;
            preparedStatement.setString(++i, resume.getFirstName());
            preparedStatement.setString(++i, resume.getLastName());
            preparedStatement.setString(++i, resume.getMiddleName());
            preparedStatement.setDate(++i, resume.getDob());
            preparedStatement.setObject(++i, resume.getSex());
            preparedStatement.setObject(++i, resume.getId());
            preparedStatement.executeUpdate();
            deleteContact(connection, resume.getId());
            deleteTechnologies(connection, resume.getId());
            resume.getContacts().forEach(contact -> saveContact(connection, resume.getId(), contact));
            resume.getTechnologies()
                    .forEach(technology -> saveTechnologies(connection, resume.getId(), technology.getId()));
            return true;
        } catch (SQLException e) {
            LOGGER.error("Request or table failed update resume ");
        }
        return false;
    }

    private void deleteTechnologies(Connection connection, Long resumeId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_RESUME_TECHNOLOGY)) {
            int i = 0;
            preparedStatement.setLong(++i, resumeId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error("Request or table failed delete technology from resume");
        }
    }

    private void deleteContact(Connection connection, Long resumeId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_RESUME_CONTACT)) {
            int i = 0;
            preparedStatement.setLong(++i, resumeId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error("Request or table failed delete contact from resume");
        }
    }

    @Override
    public boolean delete(Long id) {
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_RESUME_BY_RESUME_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.error("Request or table failed delete resume with id = " + id);
        }
        return false;
    }

    @Override
    public Resume getById(Long id) {
        Resume resume = new Resume();
        try (Connection connection = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_RESUME_BY_RESUME_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                resume = parseResultSet(resultSet);
                resume.setContacts(findContactsForResume(connection, resume.getId()));
                resume.setTechnologies(findTechnologyForResume(connection, resume.getId()));
            }
            return resume;
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get resume  with id = " + id);
        }
        return resume;
    }

    @Override
    public List findAll() {
        List<Resume> allResumeList = new ArrayList<>();
        try (Connection con = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(GET_ALL_RESUME)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Resume resume = parseResultSet(resultSet);
                resume.setContacts(findContactsForResume(con, resume.getId()));
                resume.setTechnologies(findTechnologyForResume(con, resume.getId()));
                allResumeList.add(resume);
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get all resume");
        }
        return allResumeList;
    }

    @Override
    public List find(Specification specification) {
        List<Resume> allResumeList = new ArrayList<>();
        try (Connection con = ConnectionPoolImpl.getInstance().getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(specification.getQuery())) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Resume resume = parseResultSet(resultSet);
                resume.setContacts(findContactsForResume(con, resume.getId()));
                resume.setTechnologies(findTechnologyForResume(con, resume.getId()));
                allResumeList.add(resume);
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get all resume by specification");
        }
        return allResumeList;
    }


    private Set<String> findContactsForResume(Connection connection, long id) {
        Set<String> contacts = new HashSet<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_CONTACTS_BY_RESUME_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                contacts.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get contacts for resume with id = " + id);
        }
        return contacts;
    }


    private Set<Technology> findTechnologyForResume(Connection connection, long id) {
        Set<Technology> technologies = new HashSet<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_TECHNOLOGY_BY_RESUME_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                technologies.add(new Technology(resultSet.getString("name")));
            }
        } catch (SQLException e) {
            LOGGER.error("Request or table failed get technology for resume with id = " + id);
        }
        return technologies;
    }

    private Resume parseResultSet(ResultSet resultSet) throws SQLException {
        try {
            long resumeId = resultSet.getLong("id");
            String firstName = resultSet.getString("first_name");
            String middleName = resultSet.getString("middle_name");
            String lastName = resultSet.getString("last_name");
            Date dob = resultSet.getDate("dob");
            Character sex = resultSet.getString("sex").charAt(0);
            return new Resume(resumeId, firstName, middleName, lastName, dob, sex);
        } catch (SQLException e) {
            LOGGER.error("Parse resultSet exception");
            throw new SQLException();

        }
    }
}