package com.todes.resume.dao;

import com.todes.resume.entity.Technology;

public interface TechnologyDao extends BaseDao<Technology, Long> {

    Technology getTechnologyByName(String name);
}
