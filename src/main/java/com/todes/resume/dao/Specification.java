package com.todes.resume.dao;

import com.todes.resume.service.SearchCriteria;

import java.util.List;

public class Specification {

    private String query;
    private List<SearchCriteria> params;

    public Specification(String query, List<SearchCriteria> params) {
        this.query = query;
        this.params = params;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<SearchCriteria> getParams() {
        return params;
    }

    public void setParams(List<SearchCriteria> params) {
        this.params = params;
    }
}
