package com.todes.resume.dao;

import com.todes.resume.entity.Resume;

import java.util.List;

public interface ResumeDao extends BaseDao<Resume, Long> {
    List<Resume> find(Specification specification);
}
