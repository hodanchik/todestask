package com.todes.resume.dao;

import java.util.List;

public interface BaseDao<ENTITY, KEY> {

    void save(ENTITY entity);

    boolean update(ENTITY entity);

    boolean delete(KEY id);

    ENTITY getById(KEY id);

    List<ENTITY> findAll();

}
