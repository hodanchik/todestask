package com.todes.resume.entity;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;

public class Resume {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "middleName")
    private String middleName;
    @Column(name = "dob" , columnDefinition = "DATE")
    private Date dob;
    @Column(name = "sex")
    private char sex;
    @ElementCollection
    private Set<String> contacts;
    @ManyToMany
    @JoinTable(
            name = "resume_technology",
            joinColumns = @JoinColumn(name = "resume_id"),
            inverseJoinColumns = @JoinColumn(name = "technology_id"))
    private Set<Technology> technologies;

    public Resume() {
    }

    public Resume(long id, String firstName, String lastName, String middleName, Date dob, char sex) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.dob = dob;
        this.sex = sex;
    }

    public Resume(long id, String firstName, String lastName, String middleName, Date dob, char sex, Set<String> contacts, Set<Technology> technologies) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.dob = dob;
        this.sex = sex;
        this.contacts = contacts;
        this.technologies = technologies;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Set<String> getContacts() {
        return contacts;
    }

    public void setContacts(Set<String> contacts) {
        this.contacts = contacts;
    }

    public Set<Technology> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(Set<Technology> technologies) {
        this.technologies = technologies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Resume)) return false;
        Resume resume = (Resume) o;
        return getId() == resume.getId() &&
                getSex() == resume.getSex() &&
                getFirstName().equals(resume.getFirstName()) &&
                getLastName().equals(resume.getLastName()) &&
                getMiddleName().equals(resume.getMiddleName()) &&
                getDob().equals(resume.getDob()) &&
                Objects.equals(getContacts(), resume.getContacts()) &&
                Objects.equals(getTechnologies(), resume.getTechnologies());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getMiddleName(), getDob(), getSex(), getContacts(), getTechnologies());
    }

    @Override
    public String toString() {
        return "Resume{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dob=" + dob +
                ", sex=" + sex +
                ", contacts=" + contacts +
                ", technologies=" + technologies +
                '}';
    }
}
