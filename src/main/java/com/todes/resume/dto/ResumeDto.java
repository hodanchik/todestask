//package com.todes.resume.dto;
//
//import com.todes.resume.entity.Technology;
//
//import java.sql.Date;
//import java.util.Set;
//
//public class ResumeDto {
//    private long id;
//    private String firstName;
//    private String lastName;
//    private String middleName;
//    private Date dob;
//    private char sex;
//    private Set<String> contacts;
//    private Set<Technology> technologies;
//
//    public ResumeDto(String firstName, String lastName, String middleName, Date dob, char sex,
//                     Set<String> contacts, Set<Technology> technologies) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.middleName = middleName;
//        this.dob = dob;
//        this.sex = sex;
//        this.contacts = contacts;
//        this.technologies = technologies;
//    }
//
//    public ResumeDto(long id, String firstName, String lastName,
//                     String middleName, Date dob, char sex, Set<String> contacts, Set<Technology> technologies) {
//        this.id = id;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.middleName = middleName;
//        this.dob = dob;
//        this.sex = sex;
//        this.contacts = contacts;
//        this.technologies = technologies;
//    }
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getMiddleName() {
//        return middleName;
//    }
//
//    public void setMiddleName(String middleName) {
//        this.middleName = middleName;
//    }
//
//    public Date getDob() {
//        return dob;
//    }
//
//    public void setDob(Date dob) {
//        this.dob = dob;
//    }
//
//    public char getSex() {
//        return sex;
//    }
//
//    public void setSex(char sex) {
//        this.sex = sex;
//    }
//
//    public Set<String> getContacts() {
//        return contacts;
//    }
//
//    public void setContacts(Set<String> contacts) {
//        this.contacts = contacts;
//    }
//
//    public Set<Technology> getTechnologies() {
//        return technologies;
//    }
//
//    public void setTechnologies(Set<Technology> technologies) {
//        this.technologies = technologies;
//    }
//
//    @Override
//    public String toString() {
//        return "ResumeDto{"+
//                " firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", middleName='" + middleName + '\'' +
//                ", dob=" + dob +
//                ", sex=" + sex +
//                ", contacts=" + contacts +
//                ", technologies=" + technologies +
//                '}';
//    }
//}
