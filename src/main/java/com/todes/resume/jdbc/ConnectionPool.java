package com.todes.resume.jdbc;

import java.sql.Connection;

public interface ConnectionPool {
    Connection getConnection();
    void closeAllConnection();
}